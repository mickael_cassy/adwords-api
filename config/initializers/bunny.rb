BUNNY_CONFIG = YAML::load(File.open("#{Rails.root}/config/bunny.yml"))[Rails.env]
$BUNNY_POOL = ConnectionPool.new(:size => 10, :timeout => 3) { Bunny.new(BUNNY_CONFIG).start }


# :host
# :port
# :user or :username
# :pass or :password
# :vhost or virtual_host
# :heartbeat or :heartbeat_interval, in seconds, default is 0 (no heartbeats). :server means "use the value from RabbitMQ config"
# :logger (Logger): The logger. If missing, one is created using :log_file and :log_level.
# :log_level (symbol or integer, default: Logger::WARN): Log level to use when creating a logger.
# :log_file (string or IO, default: STDOUT): log file or IO object to use when creating a logger
# :automatically_recover (boolean, default: true): when false, will disable automatic network failure recovery
# :network_recovery_interval (number, default: ``): interval between reconnection attempts
# :threaded (boolean): switches to single-threaded connections when set to false. Only recommended for apps that only publish messages.
# :continuation_timeout (integer): timeout for client operations that expect a response (e.g. Bunny::Queue#get), in milliseconds. Default is 4000 ms.


