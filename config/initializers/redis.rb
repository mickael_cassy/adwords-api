REDIS_CONFIG = YAML::load(File.open("#{Rails.root}/config/redis.yml"))


REDIS_CACHE_CONFIG = REDIS_CONFIG['caching'][Rails.env]

$REDIS_CACHE_POOL = ConnectionPool.new(:size => 50, :timeout => 3) {
  Redis.new(:host => REDIS_CACHE_CONFIG['host'], :port => REDIS_CACHE_CONFIG['port'])
}

