raw_yaml = YAML.load(File.read(File.join(Rails.root, '/config/adwords_api.yml')))
ADWORDS_API = raw_yaml.merge(raw_yaml[Rails.env || 'default'])
