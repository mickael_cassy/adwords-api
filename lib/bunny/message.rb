module Bunny
 class Message

    def self.publish queue, message
      $BUNNY_POOL.with do |conn|
        # open a channel
        ch = conn.create_channel
        # declare a queue
        q  = ch.queue(queue, durable: true)
        # publish a message to the default exchange which then gets routed to this queue
        q.publish(message)
        # close the connection
        # conn.stop
      end
    end

 end
end
