module Adwords
  module Tools
    class Reports

      # attr_accessor :partial_failure

      def initialize(api, config, version)
        @api = api
        @config = config
        @version = version
        @report_definition_template = {
          :selector => {:fields => []},
          :report_name => 'AdWords on Rails report',
          :report_type => nil,
          :download_format => nil,
          :date_range_type => 'LAST_7_DAYS',
          :include_zero_impressions => false
        }
        @client_customer_id = @config[:authentication][:client_customer_id]
      end

      def setClient id
        @client_customer_id = id
        self
      end

      def gather report_name, download_format, report_type, fields, date_range_type, include_zero_impressions
        validate_data(download_format, report_type)
        custom_definition = { report_name:report_name,
                              download_format:download_format,
                              report_type:report_type,
                              fields:fields,
                              date_range_type:date_range_type,
                              include_zero_impressions:include_zero_impressions
                            }
        @definition = Models::Report.create_definition(@report_definition_template, custom_definition)
        run
      end

      private

      def run
        @api.credential_handler().set_credential(:client_customer_id, @client_customer_id)
        report_utils = @api.report_utils()
        report_data = nil
        begin
          # Here we only expect reports that fit into memory. For large reports
          # you may want to save them to files and serve separately.
          report_data = report_utils.download_report(@definition)
          # format = Models::ReportFormat.report_format_for_type(@definition[:download_format])
          # content_type = format.content_type
          # filename = format.file_name(@params[:type])
          # send_data(report_data, {:filename => filename, :type => content_type})
        rescue AdwordsApi::Errors::ReportXmlError, AdwordsApi::Errors::ApiException => e
          puts '--An error happened--'
          puts e.message
        end
        report_data
      end

      def validate_data(download_format, report_type)
        format = Models::ReportFormat.report_format_for_type(download_format)
        raise StandardError, 'Unknown format' if format.nil?
        report = Models::Report.report_for_type(report_type)
        raise StandardError, 'Unknown report type' if report.nil?
      end


    end
  end
end
