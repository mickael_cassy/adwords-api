module Adwords
  module Tools
    class Awql

      # attr_accessor :partial_failure

      def initialize(api, config, version)
        @api = api
        @version = version
        @config = config
        @client_customer_id = @config[:authentication][:client_customer_id]
      end

      def setClient id
        @client_customer_id = id
        self
      end

      def gather awql, download_format
        validate_data(download_format)
        response = request(awql, download_format)
        Models::ReportResponse.new(response)
      end


      private

      def request(awql, download_format)
        @api.credential_handler().set_credential(:client_customer_id, @client_customer_id)
        result = nil
        begin
          report_utils = @api.report_utils()
          result = report_utils.download_report_with_awql(awql, download_format, @client_customer_id)
        rescue AdwordsApi::Errors::ReportXmlError, AdwordsApi::Errors::ApiException => e
          puts 'ERROR --> parameters were: '
          puts 'version: '+@version.to_s
          puts 'awql: '+awql.to_s
          puts 'download_format: '+download_format.to_s
          puts 'client_customer_id: '+@client_customer_id.to_s
          puts ("Exception occurred: %s\n%s" % [e.to_s, e.message])
          throw 'API request failed with an error, see logs for details'
        end
        result
      end

      def validate_data(download_format)
        format = Models::ReportFormat.report_format_for_type(download_format)
        raise StandardError, 'Unknown format' if format.nil?
      end

    end
  end
end
