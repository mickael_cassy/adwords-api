module Adwords
  module Tools
    class Selector

      # attr_accessor :partial_failure

      def initialize(api, config, version)
        @api = api
        @version = version
        @config = config
        @selector = nil
        @service_type = nil
        @client_customer_id = @config[:authentication][:client_customer_id]
      end

      def accounts
        @service_type = :ManagedCustomerService
        self
      end

      def campaigns
        @service_type = :CampaignService
        self
      end

      def adgroups
        @service_type = :AdGroupService
        self
      end

      def setClient id
        @client_customer_id = id
        self
      end


      def gather selector={}
        @selector = selector
        request
      end

      private

      def request
        @api.credential_handler().set_credential(:client_customer_id, @client_customer_id)
        service = @api.service(@service_type, @version)
        result = nil
        begin
          result = service.get(@selector)
        rescue AdwordsApi::Errors::ReportXmlError, AdwordsApi::Errors::ApiException => e
        # rescue AdwordsApi::Errors::ApiException => e
          puts 'ERROR --> parameters were: '
          puts 'version: '+@version.to_s
          puts 'selector: '+@selector.to_s
          puts 'service_type: '+@service_type.to_s
          puts 'client_customer_id: '+@client_customer_id.to_s
          puts ("Exception occurred: %s\n%s" % [e.to_s, e.message])
          throw 'API request failed with an error, see logs for details'
        end
        result
      end

    end
  end
end
