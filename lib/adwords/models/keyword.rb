module Adwords
  module Models
    class Keyword
      ATTRIBUTES = [# 'AccountCurrencyCode',
                    'AccountDescriptiveName',
                    # 'AccountTimeZoneId',
                    'AdGroupId',
                    'AdGroupName',
                    'AdGroupStatus',
                    # 'AdNetworkType1', # Segment
                    # 'AdNetworkType2', # Segment
                    # 'AdvertiserExperimentSegmentationBin', # Segment
                    'ApprovalStatus',
                    'AssistClicks',
                    'AssistImpressions',
                    'AssistImpressionsOverLastClicks',
                    'AverageCpc',
                    'AverageCpm',
                    'AveragePageviews',
                    'AveragePosition',
                    'AverageTimeOnSite',
                    'BidType',
                    # 'BiddingStrategyId',
                    # 'BiddingStrategyName',
                    # 'BiddingStrategyType',
                    'BounceRate',
                    'CampaignId',
                    'CampaignName',
                    'CampaignStatus',
                    'ClickAssistedConversionValue',
                    'ClickAssistedConversions',
                    'ClickAssistedConversionsOverLastClickConversions',
                    # 'ClickSignificance',
                    # 'ClickType', # Segment
                    'Clicks',
                    # 'ConversionCategoryName', # Segment
                    # 'ConversionManyPerClickSignificance',
                    'ConversionRate',
                    'ConversionRateManyPerClick',
                    # 'ConversionRateManyPerClickSignificance',
                    # 'ConversionRateSignificance',
                    # 'ConversionSignificance',
                    # 'ConversionTypeName',
                    'ConversionValue',
                    'Conversions',
                    'ConversionsManyPerClick',
                    'Cost',
                    'CostPerConversion',
                    'CostPerConversionManyPerClick',
                    # 'CostPerConversionManyPerClickSignificance',
                    # 'CostPerConversionSignificance',
                    # 'CostSignificance',
                    'CpcBid',
                    # 'CpcBidSource',
                    # 'CpcSignificance',
                    'CpmBid',
                    # 'CpmSignificance',
                    'CriteriaDestinationUrl',
                    'Ctr',
                    # 'CtrSignificance',
                    'CustomerDescriptiveName',
                    # 'Date', # Segment
                    # 'DayOfWeek', # Segment
                    # 'DestinationUrl', # nop nop nop
                    # 'Device', # Segment
                    'EnhancedCpcEnabled',
                    'ExternalCustomerId',
                    # 'FinalAppUrls',
                    # 'FinalMobileUrls',
                    # 'FinalUrls',
                    'FirstPageCpc',
                    'Id',
                    'ImpressionAssistedConversionValue',
                    'ImpressionAssistedConversions',
                    'ImpressionAssistedConversionsOverLastClickConversions',
                    # 'ImpressionSignificance',
                    'Impressions',
                    'IsNegative',
                    'KeywordMatchType',
                    'KeywordText',
                    'LabelIds',
                    'Labels',
                    # 'Month', # Segment
                    # 'MonthOfYear', # Segment
                    'PercentNewVisitors',
                    'PlacementUrl',
                    # 'PositionSignificance',
                    'PrimaryCompanyName',
                    'QualityScore',
                    # 'Quarter', # Segment
                    'SearchExactMatchImpressionShare',
                    'SearchImpressionShare',
                    'SearchRankLostImpressionShare',
                    # 'Slot', # Segment
                    'Status',
                    'TopOfPageCpc',
                    'TrackingUrlTemplate',
                    'UrlCustomParameters',
                    'ValuePerConversion',
                    'ValuePerConversionManyPerClick',
                    'ViewThroughConversions',
                    # 'ViewThroughConversionsSignificance',
                    # 'Week', # Segment
                    # 'Year' # Segment
                    ]

      def self.lcase_attrs
        ATTRIBUTES.map(&:downcase)
      end

    end
  end
end
