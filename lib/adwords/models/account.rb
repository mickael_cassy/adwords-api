module Adwords
  module Models
    class Account
      ATTRIBUTES = [# 'AccountCurrencyCode',
                    'AccountDescriptiveName',
                    'AccountTimeZoneId',
                    # 'AdNetworkType1',
                    # 'AdNetworkType2',
                    'AverageCpc',
                    'AverageCpm',
                    'AveragePosition',
                    # 'ClickType',
                    'Clicks',
                    'ContentBudgetLostImpressionShare',
                    'ContentImpressionShare',
                    'ContentRankLostImpressionShare',
                    # 'ConversionCategoryName',
                    'ConversionRate',
                    'ConversionRateManyPerClick',
                    # 'ConversionTrackerId',
                    # 'ConversionTypeName',
                    'ConversionValue',
                    'Conversions',
                    'ConversionsManyPerClick',
                    'Cost',
                    'CostPerConversion',
                    'CostPerConversionManyPerClick',
                    'CostPerEstimatedTotalConversion',
                    'Ctr',
                    'CustomerDescriptiveName',
                    # 'Date',
                    # 'DayOfWeek',
                    # 'Device',
                    'EstimatedCrossDeviceConversions',
                    'EstimatedTotalConversionRate',
                    'EstimatedTotalConversionValue',
                    'EstimatedTotalConversionValuePerClick',
                    'EstimatedTotalConversionValuePerCost',
                    'EstimatedTotalConversions',
                    'ExternalCustomerId',
                    # 'HourOfDay',
                    'Impressions',
                    'InvalidClickRate',
                    'InvalidClicks',
                    # 'Month',
                    # 'MonthOfYear',
                    'PrimaryCompanyName',
                    # 'Quarter',
                    'SearchBudgetLostImpressionShare',
                    'SearchExactMatchImpressionShare',
                    'SearchImpressionShare',
                    'SearchRankLostImpressionShare',
                    # 'Slot',
                    'ValuePerConversion',
                    'ValuePerConversionManyPerClick',
                    'ValuePerEstimatedTotalConversion',
                    'ViewThroughConversions',
                    # 'Week',
                    # 'Year'
                    ]

      def self.lcase_attrs
        ATTRIBUTES.map(&:downcase)
      end

    end
  end
end
