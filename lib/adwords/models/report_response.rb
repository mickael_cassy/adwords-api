require 'csv'

module Adwords
  module Models
    class ReportResponse

      attr_accessor :raw, :data, :header, :class_type

      def initialize(raw)
        @raw = raw
        @data = nil
        @header = nil
        @class_type = nil
      end

      def format header, class_type=nil, &block
        if @data.nil? || header != @header || (class_type != @class_type && class_type != nil)
          num_line = 1
          @header = header
          @class_type = class_type
          @data = []
          # fields = []
          CSV.parse(@raw) do |row|
            # title = row if (num_line == 1)
            # fields = row.clone if (num_line == 2)
            # total = row if (row.first.downcase == 'total')
            if (num_line > 2) && (row.first.downcase != 'total')
              # puts ']]]]]]]]]]]]]]]]]]]'
              # puts Hash[header.keys.zip(fields)].to_json
              # puts ']]]]]]]]]]]]]]]]]]]'
              @data << format_with(row, header, class_type, block)
            end
            num_line += 1
          end
        end
        @data
      end

      private

      def format_with row, header, class_type, block
        attributes = Hash[header.keys.zip(row)]
        check_types(attributes, header)
        attributes = block.call(attributes) unless block.nil?
        if class_type.nil?
          attributes
        else
          class_type.new(attributes.select{ |key, value| class_type.column_names.include?(key.to_sym) })
        end
      end

      def check_types attributes, header
        attributes.each do |name, raw_value|
          # puts name.to_s + " => " + raw_value.to_s + "(" + raw_value.class.to_s + ")(" + header[name].to_s + ")"
          raw_value = nil if raw_value == ' --'
          raw_value = raw_value.tr('%', '') unless raw_value.nil?
          type = header[name]
          if (type.nil?)
            value = raw_value
          else
            begin
              # puts " --> " + name.to_s + " => " + raw_value.to_s + "(" + type.to_s + ")"
              value = type.call(raw_value)
              # puts "===[" + value.class.to_s + "]"
            rescue TypeError => e
              # puts 'TypeError (ReportResponse.rb)'
              # puts name.to_s + ' (' + raw_value.to_s + ') is a ' + raw_value.class.to_s + ', and can\'t be casted as a ' + type.to_s
              # puts 'trying to replace it by ' + type.to_s + '.cast(0)'
              value = type.call(0)
            end
          end
          attributes[name] = value
        end
      end

    end
  end
end
