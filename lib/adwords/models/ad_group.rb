module Adwords
  module Models
    class AdGroup
      ATTRIBUTES = [#'AccountCurrencyCode',
                    'AccountDescriptiveName',
                    #'AccountTimeZoneId',
                    'AdGroupId',
                    'AdGroupName',
                    'AdGroupStatus',
                    # 'AdNetworkType1',
                    # 'AdNetworkType2',
                    # 'AdvertiserExperimentSegmentationBin',
                    'AssistClicks',
                    'AssistImpressions',
                    'AssistImpressionsOverLastClicks',
                    'AverageCpc',
                    'AverageCpm',
                    'AveragePageviews',
                    'AveragePosition',
                    'AverageTimeOnSite',
                    'AvgCostForOfflineInteraction',
                    'BidType',
                    # 'BiddingStrategyId',
                    # 'BiddingStrategyName',
                    # 'BiddingStrategyType',
                    'BounceRate',
                    'CampaignId',
                    'CampaignName',
                    'CampaignStatus',
                    'ClickAssistedConversionValue',
                    'ClickAssistedConversions',
                    'ClickAssistedConversionsOverLastClickConversions',
                    # 'ClickSignificance',
                    # 'ClickType',
                    'Clicks',
                    # 'ContentBidCriterionTypeGroup',
                    'ContentImpressionShare',
                    'ContentRankLostImpressionShare',
                    # 'ConversionCategoryName',
                    # 'ConversionManyPerClickSignificance',
                    'ConversionRate',
                    'ConversionRateManyPerClick',
                    # 'ConversionRateManyPerClickSignificance',
                    # 'ConversionRateSignificance',
                    # 'ConversionSignificance',
                    # 'ConversionTypeName',
                    'ConversionValue',
                    'Conversions',
                    'ConversionsManyPerClick',
                    'Cost',
                    'CostPerConversion',
                    'CostPerConversionManyPerClick',
                    # 'CostPerConversionManyPerClickSignificance',
                    # 'CostPerConversionSignificance',
                    'CostPerEstimatedTotalConversion',
                    # 'CostSignificance',
                    'CpcBid',
                    # 'CpcSignificance',
                    'CpmBid',
                    # 'CpmSignificance',
                    'Ctr',
                    # 'CtrSignificance',
                    'CustomerDescriptiveName',
                    # 'Date',
                    # 'DayOfWeek',
                    # 'Device',
                    'EnhancedCpcEnabled',
                    'EstimatedCrossDeviceConversions',
                    'EstimatedTotalConversionRate',
                    'EstimatedTotalConversionValue',
                    'EstimatedTotalConversionValuePerClick',
                    'EstimatedTotalConversionValuePerCost',
                    'EstimatedTotalConversions',
                    'ExternalCustomerId',
                    # 'HourOfDay',
                    'Id',
                    'ImpressionAssistedConversionValue',
                    'ImpressionAssistedConversions',
                    'ImpressionAssistedConversionsOverLastClickConversions',
                    # 'ImpressionSignificance',
                    'Impressions',
                    'LabelIds',
                    'Labels',
                    # 'Month',
                    # 'MonthOfYear',
                    'NumOfflineImpressions',
                    'NumOfflineInteractions',
                    'OfflineInteractionCost',
                    'OfflineInteractionRate',
                    'PercentNewVisitors',
                    # 'PositionSignificance',
                    'PrimaryCompanyName',
                    # 'Quarter',
                    'RelativeCtr',
                    'SearchExactMatchImpressionShare',
                    'SearchImpressionShare',
                    'SearchRankLostImpressionShare',
                    # 'Slot',
                    'TargetCpa',
                    'TotalCost',
                    'TrackingUrlTemplate',
                    'UrlCustomParameters',
                    'ValuePerConversion',
                    'ValuePerConversionManyPerClick',
                    'ValuePerEstimatedTotalConversion',
                    'ViewThroughConversions',
                    # 'ViewThroughConversionsSignificance',
                    # 'Week',
                    # 'Year'
                    ]

      def self.lcase_attrs
        ATTRIBUTES.map(&:downcase)
      end

    end
  end
end
