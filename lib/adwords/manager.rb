require 'adwords_api'
require 'adwords_api/utils'
# require 'adwords/tools/selector'

module Adwords
  class Manager

    attr_reader :selector, :reports, :awql, :token
    VERSION = :v201409

    def initialize(client, credential)

      oauth2_client_id = credential.client_id.to_s
      oauth2_client_secret = credential.client_secret
      developer_token = client.developerToken
      client_customer_id = client.id.to_s


      @token    = nil
      @config   = ADWORDS_API
      @config[:authentication] = {
        oauth2_client_id:      oauth2_client_id,
        oauth2_client_secret:  oauth2_client_secret,
        developer_token:       developer_token,
        client_customer_id:    client_customer_id
      }
      @key      = @config[:authentication][:client_customer_id] + credential.id.to_s
      get_adwords_api
      @selector = Tools::Selector.new(@api, @config, VERSION)
      @reports  = Tools::Reports.new(@api, @config, VERSION)
      @awql     = Tools::Awql.new(@api, @config, VERSION)
    end

    def get_adwords_api
      @api ||= create_api
      return @api
    end

    def refresh
      refreshToken
    end

    def valid?
      token_valid?
    end

    def self.utils
      AdwordsApi::Utils
    end
    def report_utils
      get_adwords_api.report_utils
    end

  private


    # Creates an instance of AdWords API class. Uses a configuration file and
    # Rails config directory.
    def create_api
      @config[:authentication][:oauth2_token] = loadToken unless token_valid?
      @api = AdwordsApi::Api.new(@config)
      return @api
    end

    def token_valid?
      return false if @token.nil?
      # token[:issued_at] ~= "2014-12-18 14:35:31 +0100"
      # 86400 => number of seconds in a day
      ending_date =  @token[:issued_at] + @token[:expires_in].to_i
      return ending_date > Time.now
    end

    def refreshToken
      if token_valid?
        puts '_ refresh valid token'
        puts '| started => '+@token[:issued_at].to_s
        puts '| ending => '+(@token[:issued_at] + @token[:expires_in].to_i).to_s
        @token = @api.authorize(@token)
      else
        puts '_ refresh invalid token'
        if @token.nil?
          puts '| nil token'
        else
          puts '| started => '+@token[:issued_at].to_s
          puts '| ending => '+(@token[:issued_at] + @token[:expires_in].to_i).to_s
        end
        auth_url_save = nil
        begin
          code = loadVerificationCode.to_s
          puts '| VerificationCode : ['+code+']'
          @token = @api.authorize({oauth2_verification_code: code})
          deleteUrlForBrowser
        rescue AdsCommon::Errors::OAuth2VerificationRequired, AdsCommon::Errors::AuthError => e
          if e.oauth_url.nil?
            puts ("Exception occurred: %s\n%s" % [e.to_s, e.message])
          else
            saveUrlForBrowser(e.oauth_url)
            puts "Hit Auth error, please navigate to URL:\n\t%s" % e.oauth_url
            puts 'log in and type the verification code.'
          end
        end
      end
      if token_valid?
        @token = @token.with_indifferent_access
        @token[:issued_at] = Time.parse(@token[:issued_at].to_s)
        saveToken
      end
    end

    def loadVerificationCode
      userCode  = nil
      key       = @key+"usertoken"
      $REDIS_CACHE_POOL.with do |redis|
        userCode = redis.GET(key)
        redis.DEL(key) unless userCode.nil?
      end
      userCode
    end
    def saveUrlForBrowser authUrl
      key       = @key+"userurl"
      $REDIS_CACHE_POOL.with do |redis|
        redis.SET(key, authUrl)
      end
    end
    def deleteUrlForBrowser
      key       = @key+"userurl"
      $REDIS_CACHE_POOL.with do |redis|
        redis.DEL(key)
      end
    end

    def loadToken
      $REDIS_CACHE_POOL.with do |redis|
        tok = redis.GET(@key)
        unless tok.nil?
          @token = JSON.parse(tok).with_indifferent_access
          @token[:issued_at] = Time.parse(@token[:issued_at].to_s)
        end
      end
      return @token
    end

    def saveToken
      $REDIS_CACHE_POOL.with do |redis|
        redis.SET(@key, @token.to_json)
      end if token_valid?
    end

  end
end
