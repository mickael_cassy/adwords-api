module Cequel
  class Keywords
    include Cequel::Record

    key :id,                                                        :bigint,      { partition: true }
    key :timestamp,                                                 :timestamp,   { order: :desc }

    column :newcpcbid,                                              :double
    column :AccountDescriptiveName,                                 :text
    column :AdGroupId,                                              :bigint
    column :AdGroupName,                                            :text
    column :AdGroupStatus,                                          :text
    column :ApprovalStatus,                                         :text
    column :AssistClicks,                                           :bigint
    column :AssistImpressions,                                      :bigint
    column :AssistImpressionsOverLastClicks,                        :double
    column :AverageCpc,                                             :double
    column :AverageCpm,                                             :double
    column :AveragePageviews,                                       :double
    column :AveragePosition,                                        :double
    column :AverageTimeOnSite,                                      :double
    column :BidType,                                                :text
    column :BounceRate,                                             :double
    column :CampaignId,                                             :bigint
    column :CampaignName,                                           :text
    column :CampaignStatus,                                         :text
    column :ClickAssistedConversionValue,                           :double
    column :ClickAssistedConversions,                               :bigint
    column :ClickAssistedConversionsOverLastClickConversions,       :double
    column :Clicks,                                                 :bigint
    column :ConversionRate,                                         :double
    column :ConversionRateManyPerClick,                             :double
    column :ConversionValue,                                        :double
    column :Conversions,                                            :bigint
    column :ConversionsManyPerClick,                                :bigint
    column :Cost,                                                   :double
    column :CostPerConversion,                                      :double
    column :CostPerConversionManyPerClick,                          :double
    column :CpcBid,                                                 :double
    column :CpmBid,                                                 :double
    column :CriteriaDestinationUrl,                                 :text
    column :Ctr,                                                    :double
    column :CustomerDescriptiveName,                                :text
    column :EnhancedCpcEnabled,                                     :boolean
    column :ExternalCustomerId,                                     :bigint
    column :FirstPageCpc,                                           :text
    column :ImpressionAssistedConversionValue,                      :double
    column :ImpressionAssistedConversions,                          :bigint
    column :ImpressionAssistedConversionsOverLastClickConversions,  :double
    column :Impressions,                                            :bigint
    column :IsNegative,                                             :boolean
    column :KeywordMatchType,                                       :text
    column :KeywordText,                                            :text
    column :LabelIds,                                               :text
    column :Labels,                                                 :text
    column :PercentNewVisitors,                                     :double
    column :PlacementUrl,                                           :text
    column :PrimaryCompanyName,                                     :text
    column :QualityScore,                                           :int
    column :SearchExactMatchImpressionShare,                        :double
    column :SearchImpressionShare,                                  :double
    column :SearchRankLostImpressionShare,                          :double
    column :Status,                                                 :text
    column :TopOfPageCpc,                                           :text
    column :TrackingUrlTemplate,                                    :text
    column :UrlCustomParameters,                                    :text
    column :ValuePerConversion,                                     :double
    column :ValuePerConversionManyPerClick,                         :double
    column :ViewThroughConversions,                                 :bigint
  end
end
