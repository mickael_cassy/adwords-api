module Cequel
  class Propositions
    include Cequel::Record

    key :id,                                                        :bigint,      { partition: true }
    key :timestamp,                                                 :timestamp,   { order: :desc }
    key :ia,                                                        :text

    column :newcpcbid,                                              :double
    column :resourceType,                                           :text
    column :influencialDatas,                                       :text

  end
end
