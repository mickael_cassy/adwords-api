module Cequel
  class Accounts
    include Cequel::Record

    key :id,                                        :bigint,      { partition: true }
    key :timestamp,                                 :timestamp,   { order: :desc }

    column :AccountDescriptiveName,                 :text
    column :AccountTimeZoneId,                      :text
    column :AverageCpc,                             :double
    column :AverageCpm,                             :double
    column :AveragePosition,                        :double
    column :Clicks,                                 :bigint
    column :ContentBudgetLostImpressionShare,       :double
    column :ContentImpressionShare,                 :double
    column :ContentRankLostImpressionShare,         :double
    column :ConversionRate,                         :double
    column :ConversionRateManyPerClick,             :double
    column :ConversionValue,                        :double
    column :Conversions,                            :bigint
    column :ConversionsManyPerClick,                :bigint
    column :Cost,                                   :double
    column :CostPerConversion,                      :double
    column :CostPerConversionManyPerClick,          :double
    column :CostPerEstimatedTotalConversion,        :double
    column :Ctr,                                    :double
    column :CustomerDescriptiveName,                :text
    column :EstimatedCrossDeviceConversions,        :bigint
    column :EstimatedTotalConversionRate,           :double
    column :EstimatedTotalConversionValue,          :double
    column :EstimatedTotalConversionValuePerClick,  :double
    column :EstimatedTotalConversionValuePerCost,   :double
    column :EstimatedTotalConversions,              :bigint
    column :Impressions,                            :bigint
    column :InvalidClickRate,                       :double
    column :InvalidClicks,                          :bigint
    column :PrimaryCompanyName,                     :text
    column :SearchBudgetLostImpressionShare,        :double
    column :SearchExactMatchImpressionShare,        :double
    column :SearchImpressionShare,                  :double
    column :SearchRankLostImpressionShare,          :double
    column :ValuePerConversion,                     :double
    column :ValuePerConversionManyPerClick,         :double
    column :ValuePerEstimatedTotalConversion,       :double
    column :ViewThroughConversions,                 :bigint

  end
end
