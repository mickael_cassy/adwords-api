module Cequel
  class Campaigns
    include Cequel::Record

    key :id,                                                        :bigint,    { partition: true }
    key :timestamp,                                                 :timestamp, { order: :desc }

    column :AccountDescriptiveName,                                 :text
    column :AdvertisingChannelSubType,                              :text
    column :AdvertisingChannelType,                                 :text
    column :Amount,                                                 :double
    column :AssistClicks,                                           :bigint
    column :AssistImpressions,                                      :bigint
    column :AssistImpressionsOverLastClicks,                        :double
    column :AverageCpc,                                             :double
    column :AverageCpm,                                             :double
    column :AveragePageviews,                                       :double
    column :AveragePosition,                                        :double
    column :AverageTimeOnSite,                                      :double
    column :AvgCostForOfflineInteraction,                           :double
    column :BidType,                                                :text
    column :BounceRate,                                             :double
    column :BudgetId,                                               :bigint
    column :CampaignName,                                           :text
    column :CampaignStatus,                                         :text
    column :ClickAssistedConversionValue,                           :double
    column :ClickAssistedConversions,                               :bigint
    column :ClickAssistedConversionsOverLastClickConversions,       :double
    column :Clicks,                                                 :bigint
    column :ContentBudgetLostImpressionShare,                       :double
    column :ContentImpressionShare,                                 :double
    column :ContentRankLostImpressionShare,                         :double
    column :ConversionRate,                                         :double
    column :ConversionRateManyPerClick,                             :double
    column :ConversionValue,                                        :double
    column :Conversions,                                            :bigint
    column :ConversionsManyPerClick,                                :bigint
    column :Cost,                                                   :double
    column :CostPerConversion,                                      :double
    column :CostPerConversionManyPerClick,                          :double
    column :CostPerEstimatedTotalConversion,                        :double
    column :Ctr,                                                    :double
    column :CustomerDescriptiveName,                                :text
    column :EnhancedCpcEnabled,                                     :boolean
    column :EstimatedCrossDeviceConversions,                        :bigint
    column :EstimatedTotalConversionRate,                           :double
    column :EstimatedTotalConversionValue,                          :double
    column :EstimatedTotalConversionValuePerClick,                  :double
    column :EstimatedTotalConversionValuePerCost,                   :double
    column :EstimatedTotalConversions,                              :bigint
    column :ExternalCustomerId,                                     :bigint
    column :ImpressionAssistedConversionValue,                      :double
    column :ImpressionAssistedConversions,                          :bigint
    column :ImpressionAssistedConversionsOverLastClickConversions,  :double
    column :Impressions,                                            :bigint
    column :InvalidClickRate,                                       :double
    column :InvalidClicks,                                          :bigint
    column :IsBudgetExplicitlyShared,                               :boolean
    column :LabelIds,                                               :text
    column :Labels,                                                 :text
    column :NumOfflineImpressions,                                  :bigint
    column :NumOfflineInteractions,                                 :bigint
    column :OfflineInteractionCost,                                 :double
    column :OfflineInteractionRate,                                 :double
    column :PercentNewVisitors,                                     :double
    column :PrimaryCompanyName,                                     :text
    column :RelativeCtr,                                            :double
    column :SearchBudgetLostImpressionShare,                        :double
    column :SearchExactMatchImpressionShare,                        :double
    column :SearchImpressionShare,                                  :double
    column :SearchRankLostImpressionShare,                          :double
    column :ServingStatus,                                          :text
    column :Status,                                                 :text
    column :TotalBudget,                                            :double
    column :TotalCost,                                              :double
    column :TrackingUrlTemplate,                                    :text
    column :UrlCustomParameters,                                    :text
    column :ValuePerConversion,                                     :double
    column :ValuePerConversionManyPerClick,                         :double
    column :ValuePerEstimatedTotalConversion,                       :double
    column :ViewThroughConversions,                                 :bigint

  end
end
