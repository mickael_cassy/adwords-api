module Activerecord
  class Campaign < ActiveRecord::Base
    has_many :ad_groups, dependent: :delete_all, autosave: true
    belongs_to :client

    validates :id, presence: true
    self.primary_key = 'id'

    scope :enabled, -> { where status: statuses[:enabled] }

    enum status: { unknown: 0, enabled: 1, paused: 2, removed: 3 }

  end
end
