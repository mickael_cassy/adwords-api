module Activerecord
  class Keyword < ActiveRecord::Base
    belongs_to :ad_group

    validates :id, presence: true
    self.primary_key = 'id'

    scope :enabled, -> { where status: statuses[:enabled] }

    enum status: { unknown: 0, enabled: 1, paused: 2, removed: 3 }

  end
end
