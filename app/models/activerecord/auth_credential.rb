module Activerecord
  class AuthCredential < ActiveRecord::Base
    has_many :clients_auth_credentials
    has_many :clients, through: :clients_auth_credentials

    enum plateform: { googleconsole: 0 }

    def self.stringify_plateform plateform
      { 0 => 'Google Developer Console'
      }.with_indifferent_access[plateform].to_s
    end

  end
end
