module Activerecord

  class Client < ActiveRecord::Base
    has_many :clients, dependent: :delete_all, autosave: true
    has_many :campaigns, dependent: :delete_all, autosave: true
    has_many :clients_auth_credentials
    has_many :auth_credentials, through: :clients_auth_credentials
    belongs_to :client #sub-manager / manager

    scope :isMcc, -> { where(isManager: true, client_id:nil) }
    scope :manager, -> { where(isManager: true);where.not(client_id:nil) }

    validates :id, presence: true
    self.primary_key = 'id'

  end

end
