module Activerecord

  class ClientsAuthCredential < ActiveRecord::Base
    belongs_to :client
    belongs_to :auth_credential
  end

end
