class RefreshToken
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence backfill: false do
    secondly(1500)
  end

  MODEL = Activerecord

  def perform(last_occurrence, current_occurrence)

    MODEL::Client.isMcc.each do |mcc|
      credentials = mcc.auth_credentials.where(plateform: 'googleconsole').where.not('clients_auth_credentials.acceptance_token' => nil)
      credentials.each do |cred|
        Adwords::Manager.new(mcc, cred).refresh
      end
    end

  end
end
