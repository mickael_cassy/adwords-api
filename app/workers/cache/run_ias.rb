module Cache
  class RunIas

    include Sidekiq::Worker

    MODEL = Activerecord

    def perform(keyword_id, timestamp)
      puts "---------------------------------------------------------------------------------"
      puts timestamp.class
      puts "Bunny::Message.publish('IsaIa', #{Oj.dump({id:keyword_id, timestamp: timestamp})})"
      puts "---------------------------------------------------------------------------------"
      Bunny::Message.publish('IsaIa', Oj.dump({id:keyword_id, timestamp: timestamp}))
    end

  end
end
