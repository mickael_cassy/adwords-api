module Cache
  class BuildArchitecture

    include Sidekiq::Worker

    MODEL = Activerecord

    def perform(client_id, timestamp)
      @campaigns = {}
      @adgroups = {}
      @client = nil
      @keywords = {}

      $REDIS_CACHE_POOL.with do |redis|

        pointer = 0
        loop do
          result = redis.HSCAN(client_id, pointer)
          pointer = result.first.to_i


          result.last.each_slice(2) do |slice|
            keywords_ids = Oj.load(slice.last)
            keywords = Cequel::Keywords.where(id: keywords_ids, timestamp:timestamp)
            keywords.each { |keyword|
              extract_client(keyword)
              extract_campaign(keyword)
              extract_adgroup(keyword)
              extract_keyword(keyword)
            }
          end
          save_with_transaction
          break if pointer == 0
        end


      end

    end

    def save_with_transaction
      ActiveRecord::Base.transaction do
        @client.save
        @campaigns.values.map(&:save)
        @adgroups.values.map(&:save)
        @keywords.values.map(&:save)
      end
    end

    def extract_client(keyword)
      value = {
        id: keyword[:externalcustomerid],
        name: keyword[:customerdescriptivename],
        companyName: keyword[:primarycompanyname],
        isManager: false,
        client_id: nil
      }
      if @client.nil?
        @client = MODEL::Client.find_or_initialize_by(id: value[:id])
      end
      @client.name = value[:name] if @client.name.nil? || @client.name.empty?
      @client.companyName = value[:companyName] if @client.companyName.nil? || @client.companyName.empty?
    end

    def extract_campaign(keyword)
      value = {
        id: keyword[:campaignid],
        name: keyword[:campaignname],
        status: keyword[:campaignstatus],
        client_id: keyword[:externalcustomerid]
      }
      if @campaigns[value[:id]].nil?
        @campaigns[value[:id]] = MODEL::Campaign.find_or_initialize_by(id: value[:id])
      end
      @campaigns[value[:id]].name       = value[:name] if @campaigns[value[:id]].name.nil? || @campaigns[value[:id]].name.empty?
      @campaigns[value[:id]].status     = MODEL::Campaign.statuses[value[:status].downcase] if @campaigns[value[:id]].status.nil?
      @campaigns[value[:id]].client_id  = value[:client_id] if @campaigns[value[:id]].client_id.nil?
    end

    def extract_adgroup(keyword)
      value = {
        id: keyword[:adgroupid],
        name: keyword[:adgroupname],
        status: keyword[:adgroupstatus],
        campaign_id: keyword[:campaignid]
      }
      if @adgroups[value[:id]].nil?
        @adgroups[value[:id]] = MODEL::AdGroup.find_or_initialize_by(id: value[:id])
      end
      @adgroups[value[:id]].name        = value[:name] if @adgroups[value[:id]].name.nil? || @adgroups[value[:id]].name.empty?
      @adgroups[value[:id]].status      =  MODEL::AdGroup.statuses[value[:status].downcase] if @adgroups[value[:id]].status.nil?
      @adgroups[value[:id]].campaign_id = value[:campaign_id] if @adgroups[value[:id]].campaign_id.nil?
    end

    def extract_keyword(keyword)
      value = {
        id: keyword.id,
        name: keyword[:keywordtext],
        status: keyword[:status],
        ad_group_id: keyword[:adgroupid]
      }
      if @keywords[value[:id]].nil?
        @keywords[value[:id]] = MODEL::Keyword.find_or_initialize_by(id: value[:id])
      end
      @keywords[value[:id]].name        = value[:name] if @keywords[value[:id]].name.nil? || @keywords[value[:id]].name.empty?
      @keywords[value[:id]].status      =  MODEL::Keyword.statuses[value[:status].downcase] if @keywords[value[:id]].status.nil?
      @keywords[value[:id]].ad_group_id = value[:ad_group_id] if @keywords[value[:id]].ad_group_id.nil?
    end


  end
end
