module Performance
  class GetAdGroupsPerformance
    include Sidekiq::Worker

    MODEL = Activerecord

    def perform(ids=[])
      adgroups = check_param(ids)
      @awql = Adwords::Manager.new.awql
      to_go = adgroups.values.flatten.count unless adgroups.empty?

      date = Date.today

      adgroups.each do |cid, adgids|
        adgids.each_slice(50) { |slice|
          puts to_go.to_s + ' Performance report to go - processing '+slice.to_s
          @awql.setClient(cid)
          handle_adgroups(slice, date)
          to_go -= slice.count
        }
      end
    end

    def check_param ids
      # SELECT `adg`.`id`, `cp`.`client_id`
      # FROM `ad_groups` AS `adg`
      # INNER JOIN (`campaigns` AS `cp`)
      # ON (`adg`.`campaign_id`=`cp`.`id`)
      # ORDER BY `cp`.`client_id` ASC
      query = MODEL::AdGroup.select(:id, :client_id).enabled.joins(:campaign).order(:campaign_id)
      unless ids.nil? || ids.empty?
        query = query.where(id: ids)
      end
      result = {}
      query.each { |adg|
        result[adg.client_id] = [] if result[adg.client_id].nil?
        result[adg.client_id] << adg.id
      }
      result
    end

    def handle_adgroups ids, date
      columns = Cequel::AdGroups.columns.map {|c| [c.to_s, c.type.method(:cast)] unless (c.to_s == 'id' || c.to_s == 'timestamp'|| c.to_s == 'newcpcbid')}.compact.to_h
      columns['AdGroupId'] = Cequel::AdGroups.columns.map{|c| c if c.name == :id}.compact.first.method(:cast)

      result = @awql.gather('SELECT ' + columns.keys.join(',') + ' FROM ADGROUP_PERFORMANCE_REPORT WHERE AdGroupId IN ' + ids.to_s + '  DURING '+ date.to_s.tr('-', '') + ',' + date.to_s.tr('-', ''), 'CSV')
      result.format(columns)
      result.data.each { |o|
        o[:id] = o['AdGroupId']
        o[:timestamp] = date.to_time.to_i
        o[:newcpcbid] = Cequel::AdGroups.columns.map{|c| c if c.name == :newcpcbid}.compact.first.cast(0)
        Cequel::AdGroups.create(o.reject{|k,v| k.to_s == 'AdGroupId'})
      }
    end

  end
end
