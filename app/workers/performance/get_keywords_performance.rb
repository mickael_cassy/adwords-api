module Performance
  class GetKeywordsPerformance
    include Sidekiq::Worker

    MODEL = Activerecord

    def perform(ids=[])
      keywords = check_param(ids)
      @awql = Adwords::Manager.new.awql
      to_go = keywords.values.flatten.count unless keywords.empty?

      date = Date.today

      keywords.each do |cid, kids|
        kids.each_slice(50) { |slice|
          puts to_go.to_s + ' Performance report to go - processing '+slice.to_s
          @awql.setClient(cid)
          handle_keywords(slice, date)
          to_go -= slice.count
        }
      end
    end

    def check_param ids
      # SELECT `k`.`id`, `cp`.`client_id`
      # FROM `keywords` as `k`
      # INNER JOIN (`campaigns` AS `cp`, `ad_groups` AS `adg`)
      # ON (`k`.`ad_group_id`=`adg`.`id` AND `adg`.`campaign_id`=`cp`.`id`)
      # ORDER BY `cp`.`id` ASC
      # ;
      query = MODEL::Keyword.select(:id, :client_id).enabled.joins(ad_group: :campaign).order('campaigns.id')
      unless ids.nil? || ids.empty?
        query = query.where(id: ids)
      end
      result = {}
      query.each { |k|
        result[k.client_id] = [] if result[k.client_id].nil?
        result[k.client_id] << k.id
      }
      result
    end

    def handle_keywords ids, date
      columns = Cequel::Keywords.columns.map {|c| [c.to_s, c.type.method(:cast)] unless (c.to_s == 'id' || c.to_s == 'timestamp'|| c.to_s == 'newcpcbid')}.compact.to_h
      columns['Id'] = Cequel::Keywords.columns.map{|c| c if c.name == :id}.compact.first.method(:cast)

      result = @awql.gather('SELECT ' + columns.keys.join(',') + ' FROM KEYWORDS_PERFORMANCE_REPORT WHERE Id IN ' + ids.to_s + '  DURING '+ date.to_s.tr('-', '') + ',' + date.to_s.tr('-', ''), 'CSV')
      result.format(columns)
      result.data.each { |o|
        o[:id] = o['Id']
        o[:timestamp] = date.to_time.to_i
        o[:newcpcbid] = Cequel::Keywords.columns.map{|c| c if c.name == :newcpcbid}.compact.first.cast(0)
        Cequel::Keywords.create(o.reject{|k,v| k.to_s == 'Id'})
      }
    end

  end
end
