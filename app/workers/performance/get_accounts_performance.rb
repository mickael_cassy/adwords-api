module Performance
  class GetAccountsPerformance
    include Sidekiq::Worker

    MODEL = Activerecord

    def perform(ids=[], timestamp)
      ids = check_param(ids)
      to_go = ids.count
      @awql = nil
      date = Time.at(timestamp).to_date

      ids.each do |id|
        id = id.to_s.tr('-','').to_i
        puts to_go.to_s + ' Performance report to go - processing ('+id.to_s+')'

        client = MODEL::Client.where(id:id).first
        manager = client.client #sub-manager
        manager = manager.client || manager # mcc
        plateform = Activerecord::AuthCredential.plateforms[:googleconsole]
        credential = manager.auth_credentials.where(plateform: plateform).where.not('clients_auth_credentials.acceptance_token' => nil).first
        unless manager.nil? || credential.nil?
          @awql = Adwords::Manager.new(manager, credential).awql
          @awql.setClient(id)
          handle_account(id, date)
        end

        to_go -= 1
      end
    end

    def check_param ids
      # SELECT `clients`.`id` FROM `clients` WHERE `clients`.`isManager` = 1
      query = MODEL::Client.select(:id).where(isManager: false)
      unless ids.nil? || ids.empty?
        # AND `clients`.`id` IN ids
        query = query.where(id: ids)
      end
      query.map(&:id) || []
    end

    def handle_account id, date
      columns = Cequel::Accounts.columns.map {|c| [c.to_s, c.type.method(:cast)] unless (c.to_s == 'id' || c.to_s == 'timestamp')}.compact.to_h
      columns[:ExternalCustomerId] = Cequel::Campaigns.columns.map{|c| c if c.name == :id}.compact.first.method(:cast)
      result = @awql.gather('SELECT ' + columns.keys.join(',') + ' FROM ACCOUNT_PERFORMANCE_REPORT  DURING '+ date.to_s.tr('-', '') + ',' + date.to_s.tr('-', ''), 'CSV')
      result.format(columns)
      result.data.each { |o|
        o[:id] = o[:ExternalCustomerId]
        o[:timestamp] = date.to_time.to_i
        Cequel::Accounts.create(o.reject{|k,v| k == :ExternalCustomerId})
      }
    end

  end
end
