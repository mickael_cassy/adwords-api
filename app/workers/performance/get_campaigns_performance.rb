module Performance
  class GetCampaignsPerformance
    include Sidekiq::Worker

    MODEL = Activerecord

    def perform(ids=[])
      campaigns = check_param(ids)
      @awql = Adwords::Manager.new.awql
      to_go = campaigns.values.flatten.count unless campaigns.empty?

      date = Date.today

      campaigns.each do |cid, campids|
        campids.each_slice(50) { |slice|
          puts to_go.to_s + ' P Performance report to go - processing ('+slice.join(',')+')'
          @awql.setClient(cid)
          handle_campaign(slice, date)
          to_go -= slice.count
        }
      end
    end

    def check_param ids
      query = MODEL::Campaign.select(:id, :client_id).enabled
      unless ids.nil? || ids.empty?
        query = query.where(id: ids)
      end
      result = {}
      query.each { |c|
        result[c.client_id] = [] if result[c.client_id].nil?
        result[c.client_id] << c.id
      }
      result
    end

    def handle_campaign ids, date
      columns = Cequel::Campaigns.columns.map {|c| [c.to_s, c.type.method(:cast)] unless (c.to_s == 'id' || c.to_s == 'timestamp')}.compact.to_h
      columns[:CampaignId] = Cequel::Campaigns.columns.map{|c| c if c.name == :id}.compact.first.method(:cast)
      result = @awql.gather('SELECT ' + columns.keys.join(',') + ' FROM CAMPAIGN_PERFORMANCE_REPORT WHERE CampaignId IN ' + ids.to_s + '  DURING '+ date.to_s.tr('-', '') + ',' + date.to_s.tr('-', ''), 'CSV')
      result.format(columns)
      result.data.each { |o|
        o[:id] = o[:CampaignId]
        o[:timestamp] = date.to_time.to_i
        Cequel::Campaigns.create(o.reject{|k,v| k == :CampaignId})
      }
    end


  end
end

# @awql = Adwords::Manager.new.awql
# date = Date.today.to_s.tr('-', '')
# @awql.setClient(2245388220)
# columns = Cequel::Campaigns.columns.map {|c| [c.to_s, c.type.method(:cast)] unless (c.to_s == 'id' || c.to_s == 'timestamp')}.compact.to_h
# result = @awql.gather('SELECT ' + columns.keys.join(',') + ' FROM CAMPAIGN_PERFORMANCE_REPORT WHERE CampaignId IN [105852801]  DURING '+ date + ',' + date, 'CSV')
# result.format(columns, Cequel::Campaigns)



# # result.raw.split("\n").each {|row| puts Hash[ result.raw.split("\n")[1].split(',').zip(row.split(",")) ], "\n" }
