class GetAccountsFromMcc
  include Sidekiq::Worker

#small sub-manager 5019577963
#mcc_effilab 179-492-1797
  MODEL = Activerecord

  def perform(ids, timestamp)
    ids = check_param(ids)
    mccs_to_go = ids.count
    @mngr = nil

    ids.each do |id|
      id = id.to_s.tr('-','').to_i
      puts mccs_to_go.to_s + ' MCC\'s to go - processing ('+id.to_s+')'

      client = MODEL::Client.where(id:id).first
      plateform = Activerecord::AuthCredential.plateforms[:googleconsole]
      credential = client.auth_credentials.where(plateform: plateform).where.not('clients_auth_credentials.acceptance_token' => nil).first
      unless client.nil? || credential.nil?
        @mngr = Adwords::Manager.new(client, credential)
        @adwords_manager = @mngr.selector.setClient(id)
        handle_accounts id
      end
      mccs_to_go -= 1
    end

    @clients.keys.each { |key|
       # if (key.to_s == '1621057591')
        Performance::GetAccountsPerformance.perform_async([key], timestamp)
        BigQuery::AllKeywordsInAccount.perform_async(key, timestamp)
      # end
    } unless @clients.nil?
  end

  def check_param ids
    if ids.nil? || ids.empty?
      # SELECT `clients`.`id` FROM `clients` WHERE `clients`.`isManager` = 1
      ids = MODEL::Client.select(:id).where(isManager: true).map(&:id)
    end
    ids || []
  end

  def handle_accounts mid

    accounts = @adwords_manager.accounts
    result = accounts.gather({fields: [:Name, :CustomerId, :CompanyName]})
    @managed_accounts = result[:entries]
    @links = result[:links]

    @tree = {}
    @links.each { |link|
      @tree[link[:manager_customer_id]] ||= []
      @tree[link[:manager_customer_id]] << link[:client_customer_id]
    } unless @links.nil?

    @accounts_to_go = @managed_accounts.count

    @manager = nil
    @clients = {}
    persist_manager(mid)
    saveClients(mid)

    @finalClients = @clients.select {|key, c| c.clients.empty?}
  end

  def persist_client id, parent
      puts @accounts_to_go.to_s + ' accounts to go - processing ('+id.to_s+')'

      acc = getAccount(id)
      @clients[id] = MODEL::Client.find_or_initialize_by(id: acc[:customer_id])
      @clients[id].name = acc[:name]||acc[:customer_id]
      @clients[id].companyName = acc[:company_name]||''
      @clients[id].client = @clients[parent]
      @clients[parent].clients << @clients[id]
      @clients[id].save

      @accounts_to_go -= 1
  end

  def persist_manager id
    puts @accounts_to_go.to_s + ' accounts to go - processing ('+id.to_s+')'

    acc = getAccount(id)
    @manager = MODEL::Client.find_or_initialize_by(id: acc[:customer_id])
    @manager.name = acc[:name]
    @manager.companyName = acc[:company_name]
    @manager.isManager = true
    @manager.save
    @clients[id] = @manager
    @accounts_to_go -= 1
  end

  def saveClients id, level=0
    unless @tree[id].nil?
      persist_manager(id)
      @tree[id].each { |i|
        persist_client(i, id)
        saveClients(i, level+1)
      }
    end
  end

  def getAccount id
    @managed_accounts.select{|elem| elem[:customer_id]==id}.first
  end

end
