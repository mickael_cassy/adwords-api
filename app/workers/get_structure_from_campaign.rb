require 'csv'

class GetStructureFromCampaign
  include Sidekiq::Worker

#small client 2180229210
#small sub-manager 5019577963
#mcc_effilab 1794921797

  MODEL = Activerecord

  def perform(ids)
    ids.map!{|id| id.to_s.tr('-','').to_i}
    all_campaigns = check_param(ids)
    @mngr = Adwords::Manager.new
    today = Date.today.to_s.tr('-','')
    campaigns_to_go = ids.count

    # Get all the AdGroups
    all_campaigns.each do |key, campaign|
      puts campaigns_to_go.to_s + ' campaigns to go - processing ('+key.to_s+')'
      # @adwords_manager = @mngr.selector.setClient(campaign.client.key) unless (campaign.client.nil? || campaign.client.key.nil?)
      awql = Adwords::Manager.new.awql
      puts 'setting client to '+campaign.client.id.to_s
      awql.setClient(campaign.client.id.to_s.tr('-','')) unless (campaign.client.nil? || campaign.client.id.nil?)
      columns = Hash[ MODEL::AdGroup.columns.map(&lambda{|v| v.name.camelize}).zip(
                      MODEL::AdGroup.columns.map{ |c| c.method(:type_cast_from_user)}
      ) ]
      result = awql.gather('SELECT '+columns.keys.joins(',')+' FROM ADGROUP_PERFORMANCE_REPORT WHERE CampaignId = '+(key.to_s)+' DURING '+today+','+today, 'CSV')
      adgroups = result.format(columns, Cequel::AdGroups)
      handle_adgroups(campaign, adgroups)
      # Get the performance reports
      active_ag = adgroups.map{|ag| ag[:id]}
      active_ag.each_slice(10) do |slice|
        ids = slice
        Performance::GetAdGroupsPerformance.perform_async(ids)
      end
      campaigns_to_go -= 1
    end

    # Get all the Keywords
    all_campaigns.each do |key, campaign|
      puts campaigns_to_go.to_s + ' campaigns to go - processing ('+key.to_s+')'
      # @adwords_manager = @mngr.selector.setClient(campaign.client.key) unless (campaign.client.nil? || campaign.client.key.nil?)
      awql = Adwords::Manager.new.awql
      awql.setClient(campaign.client.id) unless (campaign.client.nil? || campaign.client.id.nil?)
      columns = Hash[ MODEL::Keyword.columns.map(&lambda{|v| v.name.camelize}).zip(
                      MODEL::Keyword.columns.map{ |c| c.method(:type_cast_from_user)}
      ) ]
      columns['CampaignId'] = Cequel::Keywords.columns.map{|c| c if c.name == :campaignid}.compact.first.method(:cast)
      result = awql.gather('SELECT '+columns.keys.joins(',')+' FROM KEYWORDS_PERFORMANCE_REPORT WHERE CampaignId = '+(key.to_s)+' DURING '+today+','+today, 'CSV')
      keywords = result.format(columns, Cequel::Keywords)
      handle_keywords(campaign, keywords)
      # Get the performance reports
      active_k = keywords.map{|k| k[:id]}
      active_k.each_slice(10) do |slice|
        ids = slice
        Performance::GetKeywordsPerformance.perform_async(ids)
      end
      campaigns_to_go -= 1
    end


  end

  def check_param ids
    all_campaigns = {}
    if ids.nil? || ids.empty?
      # If they have no clients, they should have campaigns
      campaigns = MODEL::Campaign.all.to_a
    else
      # Getting all the known campaigns
      campaigns = MODEL::Campaign.where(id: ids).to_a
      # NOPE, Campaigns must exist in our db, if they don't refresh the account campaigns list
      # # Creating the unknown ones
      # ids.reject{|id| campaigns.select{|c| c.key.to_s == id.to_s}.count == 1}.each do |unkown_id|
      #   campaign = Campaign.find_or_initialize_by(key: unkown_id)
      #   # campaign.save
      #   campaigns << campaign
      # end
    end
    campaigns.each{ |c| all_campaigns[c.id] = c }
    all_campaigns
  end

  def handle_adgroups campaign, adgroups
    adgroups_to_go = adgroups.count

    adgroups.each do |adgroup|
      puts adgroups_to_go.to_s + ' adgroups to go  - processing('+adgroup.id.to_s+')'

      a = MODEL::AdGroup.find_or_initialize_by(id: adgroup.id)
      a.name = adgroup.name
      a.status = adgroup.status.downcase
      a.campaign = campaign
      a.save

      adgroups_to_go -= 1
    end unless (adgroups.nil? || adgroups.empty?)
  end

  def handle_keywords campaign, keywords
    keywords_to_go = keywords.count

    keywords.each do |keyword|
      puts keywords_to_go.to_s + ' keywords to go  - processing('+keyword.id.to_s+')'

      a = MODEL::Keyword.find_or_initialize_by(id: keyword.id)
      a.name = keyword.keywordtext
      a.status = keyword.status.downcase
      a.ad_group = campaign.ad_groups.select{ |ag| ag.id.to_s == keyword.adgroupid.to_s }.first
      a.save

      keywords_to_go -= 1
    end unless (keywords.nil? || keywords.empty?)
  end


end
