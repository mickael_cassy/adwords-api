class GetAccountStructure
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options :queue => :critical

#small sub-manager 5019577963
#mcc_effilab 179-492-1797

  MODEL = Activerecord

  recurrence backfill: true do
    daily.hour_of_day(2)
  end

  def perform(last_occurrence, current_occurrence)
    clients_id = MODEL::Client.select(:id).where(isManager: true).where(client: nil).map(&:id)
    clients_id.each { |id|
      GetAccountsFromMcc.perform_async([id], current_occurrence)
    }
  end


end
