class GetCampaignsFromAccount
  include Sidekiq::Worker

#small client 2180229210
#small sub-manager 5019577963
#mcc_effilab 1794921797
  MODEL = Activerecord

  def perform(ids)
    ids.map!{|id| id.to_s.tr('-','').to_i}
    all_clients = check_param(ids)
    @mngr = Adwords::Manager.new
    accounts_to_go = ids.count
    @campaignsids = []

    all_clients.each do |key, client|
      puts accounts_to_go.to_s + ' accounts to go - processing ('+key.to_s+')'
      @adwords_manager = @mngr.selector.setClient(key)
      handle_campaigns client
      accounts_to_go -= 1
    end

    @campaignsids.each_slice(10) { |slice|
      Performance::GetCampaignsPerformance.perform_async(ids)
    }
  end

  def check_param ids
    all_clients = {}
    if ids.nil? || ids.empty?
      # SELECT DISTINCT `campaigns`.`client_id` FROM `campaigns`
      clients = MODEL::Campaign.select(:client_id).uniq.map(&:client_id)
    else
      # Getting all the known clients
      clients = MODEL::Client.where(id: ids)
      # # Creating the unknown ones
      # ids.reject{|id| clients.select{|c| c.id.to_s == id.to_s}.count == 1}.each do |unkown_id|
      #   clients << MODEL::Clients.find_or_initialize_by(id: unkown_id)
      # end
    end
    clients.each{ |c| all_clients[c.id] = c }
    all_clients
  end


  def handle_campaigns client
    campaigns = @adwords_manager.campaigns.gather({:fields => [:Id, :Name, :Status]})

    campaigns_to_go = campaigns[:entries].count unless (campaigns[:total_num_entries] == 0)

    campaigns[:entries].each { |campaign|
      puts campaigns_to_go.to_s + ' campaigns to go - processing ('+campaign[:id].to_s+')'
      c = MODEL::Campaign.find_or_initialize_by(id: campaign[:id])
      c.name = campaign[:name]
      c.status = campaign[:status].downcase
      c.client = client
      c.save
      GetStructureFromCampaign.perform_async([campaign[:id]])
      @campaignsids << campaign[:id]
      campaigns_to_go -= 1

    } unless (campaigns[:total_num_entries] == 0)
  end



end
