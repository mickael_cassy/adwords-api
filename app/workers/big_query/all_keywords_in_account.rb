module BigQuery
  class AllKeywordsInAccount
    include Sidekiq::Worker

    MODEL = Activerecord

    def perform(id, timestamp)
      unless id.nil?
        @client = get_client(id)

        unless @client.nil? || @client.isManager
          manager = @client.client #sub-manager
          manager = manager.client || manager # mcc
          plateform = Activerecord::AuthCredential.plateforms[:googleconsole]
          credential = manager.auth_credentials.where(plateform: plateform).where.not('clients_auth_credentials.acceptance_token' => nil).first

          unless manager.nil? || credential.nil?
            @awql = Adwords::Manager.new(manager, credential).awql
            @awql.setClient(@client.id)

            date = Time.at(timestamp).to_date
            result = @awql.gather(' SELECT ' + columns.keys.join(',') +
                                  ' FROM KEYWORDS_PERFORMANCE_REPORT' +
                                  ' DURING '+ date.to_s.tr('-', '') + ',' + date.to_s.tr('-', ''),
                                  'CSV')
            campaigns = {}
            uniq = []
            result.format(columns) do |entrie|
              entrie[:id] = entrie['Id']
              entrie[:timestamp] = date.to_time.to_i
              entrie[:newcpcbid] = Cequel::Keywords.columns.map{|c| c if c.name == :newcpcbid}.compact.first.cast(0)
              k = Cequel::Keywords.create(entrie.reject{|k,v| k.to_s == 'Id'})
              campaigns[k[:CampaignId]] = [] if campaigns[k[:CampaignId]].nil?
              campaigns[k[:CampaignId]] << k.id
              uniq
            end
            result.data = nil

            cache(@client, campaigns)
            @client.save
            Cache::BuildArchitecture.perform_async(@client.id, timestamp)
          end
        end
      end
    end

    def get_client(id)
      client = nil
      begin
        client = MODEL::Client.find(id)
      rescue ActiveRecord::RecordNotFound => e
        puts 'Customer ('+id.to_s+') RecordNotFound.'
      end
      client
    end

    def columns
      if @column.nil?
        @columns = Cequel::Keywords.columns.map {|c| [c.to_s, c.type.method(:cast)] unless (c.to_s == 'id' || c.to_s == 'timestamp'|| c.to_s == 'newcpcbid')}.compact.to_h
        @columns['Id'] = Cequel::Keywords.columns.map{|c| c if c.name == :id}.compact.first.method(:cast)
      end
      @columns
    end

    def cache(client, campaigns)
      $REDIS_CACHE_POOL.with do |redis|
        campaigns.each do |campaign_id, keywords|
          index = 0
          keywords.each_slice(200) do |slice|
            redis.HSET(client.id, index, Oj.dump(slice))
            index += 1
          end
        end
      end
    end

  end
end
