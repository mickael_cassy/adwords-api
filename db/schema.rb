# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150129090443) do

  create_table "ad_groups", force: :cascade do |t|
    t.string  "name",        limit: 255, null: false
    t.integer "status",      limit: 1,   null: false
    t.integer "campaign_id", limit: 8,   null: false
  end

  add_index "ad_groups", ["campaign_id"], name: "index_ad_groups_on_campaign_id", using: :btree

  create_table "auth_credentials", force: :cascade do |t|
    t.integer "plateform",     limit: 1,   null: false
    t.string  "name",          limit: 255, null: false
    t.string  "client_id",     limit: 255, null: false
    t.string  "client_secret", limit: 255, null: false
    t.string  "other",         limit: 255
  end

  create_table "campaigns", force: :cascade do |t|
    t.string  "name",      limit: 255, null: false
    t.integer "status",    limit: 1,   null: false
    t.integer "client_id", limit: 8,   null: false
  end

  add_index "campaigns", ["client_id"], name: "index_campaigns_on_client_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string  "name",           limit: 255
    t.string  "companyName",    limit: 255
    t.boolean "isManager",      limit: 1,   default: false, null: false
    t.integer "client_id",      limit: 8
    t.string  "developerToken", limit: 255
  end

  add_index "clients", ["client_id"], name: "fk_rails_c763fe93cb", using: :btree

  create_table "clients_auth_credentials", id: false, force: :cascade do |t|
    t.integer "client_id",          limit: 8
    t.integer "auth_credential_id", limit: 4
    t.text    "acceptance_token",   limit: 65535
  end

  add_index "clients_auth_credentials", ["auth_credential_id"], name: "index_clients_auth_credentials_on_auth_credential_id", using: :btree
  add_index "clients_auth_credentials", ["client_id"], name: "index_clients_auth_credentials_on_client_id", using: :btree

  create_table "keywords", force: :cascade do |t|
    t.string  "name",        limit: 255, null: false
    t.integer "status",      limit: 1,   null: false
    t.integer "ad_group_id", limit: 8,   null: false
  end

  add_index "keywords", ["ad_group_id"], name: "fk_rails_13a04dd1b8", using: :btree

  add_foreign_key "ad_groups", "campaigns"
  add_foreign_key "campaigns", "clients"
  add_foreign_key "clients", "clients"
  add_foreign_key "keywords", "ad_groups"
end
