class CreateAuthCredentials < ActiveRecord::Migration
  def change
    create_table :auth_credentials do |t|
      t.integer :plateform,     null: false, limit: 1
      t.string  :name,          null: false
      t.string  :client_id,     null: false
      t.string  :client_secret, null: false
      t.string  :other,         null: true
    end
  end
end
