class CreateAdGroups < ActiveRecord::Migration
  def change
    create_table :ad_groups do |t|
      t.string  :name, null: false
      t.integer :status, null: false, limit: 1
      t.integer :campaign_id, limit: 8, null: false, index: true
    end
    change_column(:ad_groups, :id, :integer, limit: 8)
    add_foreign_key(:ad_groups, :campaigns, dependent: :delete)
  end
end
