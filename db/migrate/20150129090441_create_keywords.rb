class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.string  :name, null: false
      t.integer :status, null: false, limit: 1
      t.integer :ad_group_id, limit: 8, null: false
    end
    change_column(:keywords, :id, :integer, limit: 8)
    add_foreign_key(:keywords, :ad_groups, dependent: :delete)
  end
end
