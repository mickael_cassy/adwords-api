class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string  :name
      t.string  :companyName
      t.boolean :isManager, null: false, default: false
      t.integer :client_id, limit: 8, null: true
      t.string  :developerToken, null: true
    end
    change_column(:clients, :id, :integer, limit: 8)
    add_foreign_key(:clients, :clients, dependent: :delete)
  end
end
