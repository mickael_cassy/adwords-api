class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string  :name, null: false
      t.integer :status, null: false, limit: 1
      t.integer :client_id, limit: 8, null: false, index: true
    end
    change_column(:campaigns, :id, :integer, limit: 8)
    add_foreign_key(:campaigns, :clients, dependent: :delete)
  end
end


