class CreateClientsAndAuthCredentials < ActiveRecord::Migration
  def change
   create_table :clients_auth_credentials, id: false do |t|
      t.belongs_to  :client,           index: true
      t.belongs_to  :auth_credential,  index: true
      t.text        :acceptance_token, null:  true
    end
    change_column(:clients_auth_credentials, :client_id,           :integer, limit: 8)
  end
end


